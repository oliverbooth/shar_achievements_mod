build:
	cargo +nightly build --target i686-pc-windows-gnu
	
release:
	cargo +nightly build --target i686-pc-windows-gnu --release

deploy DIR: release
	cp ./target/i686-pc-windows-gnu/release/shar_achievements.dll {{DIR}}
	cp ./target/i686-pc-windows-gnu/release/shar_achievements.exe {{DIR}}

clippy:
	cargo +nightly clippy --target i686-pc-windows-gnu
