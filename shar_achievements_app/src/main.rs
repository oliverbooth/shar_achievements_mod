#![windows_subsystem = "windows"]

use eframe::NativeOptions;
use egui::Vec2;
use ui::AchievementApp;

mod achievements;
mod ui;

fn main() {
    let native_options = NativeOptions {
        initial_window_size: Some(Vec2 { x: 500.0, y: 900.0 }),
        ..Default::default()
    };

    eframe::run_native(
        "SHAR Achievements",
        native_options,
        Box::new(|_| Box::new(AchievementApp::new())),
    );
}
