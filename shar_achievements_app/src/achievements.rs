use std::{fmt::Display, fs::read_to_string};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Achievement {
    pub name: String,
    #[serde(skip)]
    pub description: String,
    pub unlocked: bool,
}

impl Achievement {
    /// Creates a new achievement from a given name and description.
    pub fn new(name: &str, description: &str) -> Self {
        Achievement {
            name: name.into(),
            description: description.into(),
            unlocked: false,
        }
    }
}

/// Possible errors when syncing achievements
pub enum SyncError {
    File(std::io::Error),
    Deserialisation(ron::error::SpannedError),
}

impl Display for SyncError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::File(e) => write!(f, "File IO Error: {}", e),
            Self::Deserialisation(e) => write!(f, "Deserialisation Error: {}", e),
        }
    }
}

/// Syncs the unlocked state of the given achievement list to the save file
pub fn sync_achievements(
    achievements: &mut [Achievement],
) -> Result<Vec<&mut Achievement>, SyncError> {
    let save_file = read_to_string("achievements.ron").map_err(SyncError::File)?;

    // Prevent returning a deserialisation error if file is empty
    let saved_achievements: Vec<Achievement> = if save_file.is_empty() {
        Vec::new()
    } else {
        ron::from_str(&save_file).map_err(SyncError::Deserialisation)?
    };

    let mut newly_unlocked_achievements = Vec::new();

    achievements.iter_mut().for_each(|a| {
        let old_unlocked = a.unlocked;

        a.unlocked = saved_achievements
            .iter()
            .find(|sa| a.name == sa.name)
            .map(|sa| sa.unlocked)
            .unwrap_or_else(|| a.unlocked);

        if a.unlocked && !old_unlocked {
            newly_unlocked_achievements.push(a);
        }
    });

    Ok(newly_unlocked_achievements)
}

/// Creates a vector of achievements based on the RetroAchievements list
/// (sans I Can't See Anything)
pub fn init_achievements() -> Vec<Achievement> {
    vec![
        Achievement::new("The Cola Caper", "Finish The Cola Caper"),
        Achievement::new("S-M-R-T", "Finish S-M-R-T"),
        Achievement::new("Petty Theft Homer", "Finish Petty Theft Homer"),
        Achievement::new("Office Spaced", "Finish Office Spaced"),
        Achievement::new("Blind Big Brother", "Finish Blind Big Brother"),
        Achievement::new("Flowers by Irene", "Finish Flowers by Irene"),
        Achievement::new("Bonestorm Storm", "Finish Bonestorm Storm"),
        Achievement::new("The Fat and Furious", "Finish The Fat and Furious"),
        Achievement::new(
            "Detention Deficit Disorder",
            "Finish Detention Deficit Disorder",
        ),
        Achievement::new(
            "Weapons of Mass Deliquency",
            "Finish Weapons of Mass Deliquency",
        ),
        Achievement::new("Vox Nerduli", "Finish Vox Nerduli"),
        Achievement::new("Bart 'n' Frink", "Finish Bart 'n' Frink"),
        Achievement::new("Better Than Beef", "Finish Better Than Beef"),
        Achievement::new("Monkey See Monkey D'oh", "Finish Monkey See Monkey D'oh"),
        Achievement::new("Cell-Outs", "Finish Cell-Outs"),
        Achievement::new("Nerd Race Queen", "Finish Nerd Race Queen"),
        Achievement::new("Clueless", "Finish Clueless"),
        Achievement::new("Bonfire of the Manatees", "Finish Bonfire of the Manatees"),
        Achievement::new("Operation Hellfish", "Finish Operation Hellfish"),
        Achievement::new("Slithery Sleuthing", "Finish Slithery Sleuthing"),
        Achievement::new("Fishy Deals", "Finish Fishy Deals"),
        Achievement::new(
            "The Old Pirate and the Sea",
            "Finish The Old Pirate and the Sea",
        ),
        Achievement::new("For a Few Donuts More", "Finish For a Few Donuts More"),
        Achievement::new("Redneck Roundup", "Finish Redneck Roundup"),
        Achievement::new("Ketchup Logic", "Finish Ketchup Logic"),
        Achievement::new(
            "Return of the Nearly Dead",
            "Finish Return of the Nearly Dead",
        ),
        Achievement::new("Wolves Stole My Pills", "Finish Wolves Stole My Pills"),
        Achievement::new("The Cola Wars", "Finish The Cola Wars"),
        Achievement::new("From Outer Space", "Finish From Outer Space"),
        Achievement::new("Incriminating Caffeine", "Finish Incriminating Caffeine"),
        Achievement::new("...And Baby Makes 8", "Finish ...And Baby Makes 8"),
        Achievement::new("Eight is Too Much", "Finish Eight is Too Much"),
        Achievement::new("This Little Piggy", "Finish This Little Piggy"),
        Achievement::new("Never Trust a Snake", "Finish Never Trust a Snake"),
        Achievement::new("Kwik Cash", "Finish Kwik Cash"),
        Achievement::new("Curious Curator", "Finish Curious Curator"),
        Achievement::new("Going to the Lu'", "Finish Going to the Lu'"),
        Achievement::new(
            "Getting Down with the Clown",
            "Finish Getting Down with the Clown",
        ),
        Achievement::new("Lab Coat Caper", "Finish Lab Coat Caper"),
        Achievement::new(
            "Duff for Me, Duff for You",
            "Finish Duff for Me, Duff for You",
        ),
        Achievement::new("Full Metal Jackass", "Finish Full Metal Jackass"),
        Achievement::new("Set to Kill", "Finish Set to Kill"),
        Achievement::new(
            "Kang and Kodos Strike Back",
            "Finish Kang and Kodos Strike Back",
        ),
        Achievement::new("Rigor Motors", "Finish Rigor Motors"),
        Achievement::new("Long Black Probes", "Finish Long Black Probes"),
        Achievement::new("Pocket Protector", "Finish Pocket Protector"),
        Achievement::new(
            "There's Something About Monty",
            "Finish There's Something About Monty",
        ),
        Achievement::new(
            "Alien \"Auto\"topsy Part I",
            "Finish Alien \"Auto\"topsy Part I",
        ),
        Achievement::new(
            "Alien \"Auto\"topsy Part II",
            "Finish Alien \"Auto\"topsy Part II",
        ),
        Achievement::new(
            "Alien \"Auto\"topsy Part III",
            "Finish Alien \"Auto\"topsy Part III",
        ),
        Achievement::new("This Old Shanty", "Finish This Old Shanty"),
        Achievement::new("Dial B for Blood", "Finish Dial B for Blood"),
        Achievement::new("Princi-Pal", "Finish Princi-Pal"),
        Achievement::new("Beached Love", "Finish Beached Love"),
        Achievement::new("Kinky Frinky", "Finish Kinky Frinky"),
        Achievement::new("Milking the Pigs", "Finish Milking the Pigs"),
        Achievement::new("Flaming Tires", "Finish Flaming Tires"),
        Achievement::new("Daytime Suburb Racing", "Finish all races from Level 1"),
        Achievement::new("Daytime Downtown Racing", "Finish all races from Level 2"),
        Achievement::new("Daytime Squidport Racing", "Finish all races from Level 3"),
        Achievement::new("Night Suburb Racing", "Finish all races from Level 4"),
        Achievement::new("Night Downtown Racing", "Finish all races from Level 5"),
        Achievement::new("Night Squidport Racing", "Finish all races from Level 6"),
        Achievement::new("Halloween Racing", "Finish all races from Level 7"),
        Achievement::new("Surveillance Van", "Unlock all vehicles from Level 1"),
        Achievement::new("WWII Vehicle", "Unlock all vehicles from Level 2"),
        Achievement::new("Book Burning Van", "Unlock all vehicles from Level 3"),
        Achievement::new("Krusty's Limo", "Unlock all vehicles from Level 4"),
        Achievement::new("El Carro Loco", "Unlock all vehicles from Level 5"),
        Achievement::new("36 Stutz Bearcat", "Unlock all vehicles from Level 6"),
        Achievement::new("Hearse", "Unlock all vehicles from Level 7"),
        Achievement::new("King Size Homer", "Buy all clothings from Level 1"),
        Achievement::new("Bart Star", "Buy all clothings from Level 2"),
        Achievement::new("Summer of 4 ft.2", "Buy all clothings from Level 3"),
        Achievement::new(
            "Scenes from the Class Struggle in Springfield",
            "Buy all clothings from Level 4",
        ),
        Achievement::new("Much Apu About Nothing", "Buy all clothings from Level 5"),
        Achievement::new("Bartman #1", "Buy all clothings from Level 6"),
        Achievement::new("Tree House of Horror - 4", "Buy all clothings from Level 7"),
        Achievement::new("Wasp Killer I", "Destroy all wasps from Level 1"),
        Achievement::new("Wasp Killer II", "Destroy all wasps from Level 2"),
        Achievement::new("Wasp Killer III", "Destroy all wasps from Level 3"),
        Achievement::new("Wasp Killer IV", "Destroy all wasps from Level 4"),
        Achievement::new("Wasp Killer V", "Destroy all wasps from Level 5"),
        Achievement::new("Wasp Killer VI", "Destroy all wasps from Level 6"),
        Achievement::new("Wasp Killer VII", "Destroy all wasps from Level 7"),
        Achievement::new("Homer Loves Flanders", "Find all gags from Level 1"),
        Achievement::new("Bart on the Road", "Find all gags from Level 2"),
        Achievement::new("Three Men and a Comic Book", "Find all gags from Level 3"),
        Achievement::new("The Old Man and the Key", "Find all gags from Level 4"),
        Achievement::new("Flaming Moe's", "Find all gags from Level 5"),
        Achievement::new("Krusty Gets Busted", "Find all gags from Level 6"),
        Achievement::new("Treehouse of Horror", "Find all gags from Level 7"),
        Achievement::new("Mr. Sparkle Box", "Collect all cards from Level 1"),
        Achievement::new("Head of Jebediah", "Collect all cards from Level 2"),
        Achievement::new("Soy Pop", "Collect all cards from Level 3"),
        Achievement::new("Boudoir Album", "Collect all cards from Level 4"),
        Achievement::new("Ganesh Costume", "Collect all cards from Level 5"),
        Achievement::new("Radioactive Man #1", "Collect all cards from Level 6"),
        Achievement::new("Soul Donut", "Collect all cards from Level 7"),
        Achievement::new(
            "Wager Racing I",
            "Get a time of 3:15 or less on Level 1 wager race",
        ),
        Achievement::new(
            "Wager Racing II",
            "Get a time of 2:10 or less on Level 2 wager race",
        ),
        Achievement::new(
            "Wager Racing III",
            "Get a time of 2:20 or less on Level 3 wager race",
        ),
        Achievement::new(
            "Wager Racing IV",
            "Get a time of 2:30 or less on Level 4 wager race",
        ),
        Achievement::new(
            "Wager Racing V",
            "Get a time of 2:05 or less on Level 5 wager race",
        ),
        Achievement::new(
            "Wager Racing VI",
            "Get a time of 1:55 or less on Level 6 wager race",
        ),
        Achievement::new(
            "Wager Racing VII",
            "Get a time of 1:30 or less on Level 7 wager race",
        ),
        Achievement::new(
            "Daytime Suburb Super Sprint",
            "Win a race on Level 1 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Daytime Downtown Super Sprint",
            "Win a race on Level 2 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Daytime Squidport Super Sprint",
            "Win a race on Level 3 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Night Suburb Super Sprint",
            "Win a race on Level 4 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Night Downtown Super Sprint",
            "Win a race on Level 5 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Night Squidport Super Sprint",
            "Win a race on Level 6 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Halloween Super Sprint",
            "Win a race on Level 7 in Bonus Game (3 laps)",
        ),
        Achievement::new(
            "Car Surfing",
            "Keep surfing on a vehicle for at least 5 seconds",
        ),
        Achievement::new(
            "Matt Groening Gaming",
            "Kick Marge all the way to Kwik E-Mart during Level 1",
        ),
        Achievement::new(
            "The Simp",
            "Push Milhouse to the cliff at the end of Clueless",
        ),
        Achievement::new("500-Yard Gash", "Unlock the bonus movie"),
        Achievement::new(
            "1970 Blue Flame",
            "Finish The Fat and the Furious using only the Speed Rocket",
        ),
        Achievement::new(
            "Marge vs. The Monorail",
            "Finish Cell-Outs using only the Monorail Car",
        ),
        Achievement::new(
            "Knightboat: The Crime Solving Boat",
            "Finish The Old Pirate and the Sea using only the Knight Boat",
        ),
        Achievement::new(
            "Quad Bike",
            "Finish Return of the Nearly-Dead using only the ATV",
        ),
        Achievement::new(
            "Monster Truck",
            "Finish ...and Baby Makes 8 using only the Obliteratatron Big Wheel Truck",
        ),
        Achievement::new(
            "Unfunny Pink Car",
            "Finish Getting Down with the Clown using only the Planet Hype 50's Car",
        ),
        Achievement::new(
            "Radio-Controlled",
            "Finish There's Something About Monty using only the R/C Buggy",
        ),
        Achievement::new(
            "Fire Truck Challenge",
            "Finish Fishy Deals using only the Fire Truck",
        ),
        Achievement::new(
            "Donut Challenge",
            "Finish For A Few Donuts More using only the Donut Truck",
        ),
        Achievement::new(
            "Duff Challenge",
            "Finish From Outer Space using only the Duff Truck",
        ),
        Achievement::new(
            "Cola Challenge",
            "Finish Duff For Me, Duff For You using only the Cola Truck",
        ),
        Achievement::new(
            "Plow King Challenge",
            "Finish Full Metal Jackass using only the Plow King",
        ),
        Achievement::new(
            "Armored Challenge",
            "Finish Rigor Motors using only the Armored Truck",
        ),
    ]
}
