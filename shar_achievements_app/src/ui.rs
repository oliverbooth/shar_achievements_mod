use crate::achievements::{init_achievements, sync_achievements, Achievement, SyncError};
use dll_syringe::{process::OwnedProcess, Syringe};
use eframe::{App, Frame};
use egui::{menu, CentralPanel, Context, RichText, ScrollArea, Sense, TopBottomPanel};
use egui_toast::{Toast, ToastKind, ToastOptions, Toasts};
use std::{
    collections::VecDeque,
    fs::File,
    io::Write,
    time::{Duration, Instant},
};

pub struct AchievementApp {
    achievements: Vec<Achievement>,
    update_save: bool,
    toast_queue: VecDeque<(ToastKind, String)>,
    last_synced: Instant,
}

impl AchievementApp {
    pub fn new() -> Self {
        let mut achievements = init_achievements();
        let mut toast_queue = VecDeque::new();

        match sync_achievements(&mut achievements) {
            Err(SyncError::File(error)) => {
                if error.kind() == std::io::ErrorKind::NotFound {
                    if let Err(error) = File::create("achievements.ron") {
                        toast_queue.push_back((ToastKind::Error, error.to_string()));
                    }
                } else {
                    toast_queue.push_back((ToastKind::Error, error.to_string()));
                }
            }
            Err(SyncError::Deserialisation(error)) => {
                toast_queue.push_back((ToastKind::Error, error.to_string()));
            }
            _ => (),
        }

        AchievementApp {
            achievements,
            toast_queue,
            ..Default::default()
        }
    }

    fn inject_dll() -> Result<(), String> {
        // Find SHAR by name
        let target_process = OwnedProcess::find_first_by_name("Simpsons")
            .ok_or("SHAR process could not be found.")?;

        // Create a new syringe for SHAR
        let syringe = Syringe::for_process(target_process);

        // Inject the DLL into SHAR
        syringe
            .inject("shar_achievements.dll")
            .map(|_| ())
            .map_err(|_| "DLL could not be injected.".into())
    }
}

impl Default for AchievementApp {
    fn default() -> Self {
        AchievementApp {
            achievements: Vec::new(),
            update_save: false,
            toast_queue: VecDeque::new(),
            last_synced: Instant::now(),
        }
    }
}

impl App for AchievementApp {
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        // Ensure that this method is run at least once a second
        ctx.request_repaint_after(Duration::from_secs(1));

        // Sync with changes made by injected DLL once a second
        if (Instant::now() - self.last_synced).as_secs() > 1 {
            match sync_achievements(&mut self.achievements) {
                Ok(achievements) => achievements
                    .iter()
                    .map(|a| format!("{} unlocked!", a.name))
                    .map(|s| (ToastKind::Success, s))
                    .for_each(|t| self.toast_queue.push_back(t)),
                Err(error) => self
                    .toast_queue
                    .push_back((ToastKind::Error, error.to_string())),
            }

            self.last_synced = Instant::now();
        }

        // Display menu bar
        TopBottomPanel::top("menu").show(ctx, |ui| {
            menu::bar(ui, |ui| {
                if ui.button("Inject DLL").clicked() {
                    match AchievementApp::inject_dll() {
                        Ok(()) => self
                            .toast_queue
                            .push_back((ToastKind::Success, "DLL successfully injected.".into())),
                        Err(error) => self.toast_queue.push_back((ToastKind::Error, error)),
                    }
                }

                if ui.button("Lock All").clicked() {
                    self.achievements
                        .iter_mut()
                        .for_each(|a| a.unlocked = false);
                    self.update_save = true;
                }

                if ui.button("Unlock All").clicked() {
                    self.achievements.iter_mut().for_each(|a| a.unlocked = true);
                    self.update_save = true;
                }
            })
        });

        // Display achievement count
        TopBottomPanel::top("count").show(ctx, |ui| {
            let unlocked_count = self.achievements.iter().filter(|a| a.unlocked).count();
            let total_count = self.achievements.len();
            let count = format!("🏆{}/{}", unlocked_count, total_count);

            ui.label(RichText::new(count).size(60.0));
        });

        // Display achievement list
        CentralPanel::default().show(ctx, |ui| {
            ScrollArea::vertical()
                .auto_shrink([false; 2])
                .show(ui, |ui| {
                    for achievement in &mut self.achievements {
                        let row = ui.push_id(&achievement.name, |ui| {
                            ui.set_enabled(achievement.unlocked);
                            ui.set_min_width(ui.available_width());

                            ui.vertical(|ui| {
                                ui.heading(&achievement.name);
                                ui.label(&achievement.description);
                            });
                        });

                        let response = row.response.interact(Sense::click());

                        if response.double_clicked() {
                            achievement.unlocked = !achievement.unlocked;
                            self.update_save = true;
                        }

                        ui.separator();
                    }
                });
        });

        // Update save file when achievements are toggled
        if self.update_save {
            use ron::ser as ron;

            if let Ok(serialised_achievements) =
                ron::to_string_pretty(&self.achievements, ron::PrettyConfig::default())
            {
                if let Err(error) = File::create("achievements.ron")
                    .and_then(|mut f| f.write_all(serialised_achievements.as_bytes()))
                    .map_err(|e| e.to_string())
                {
                    self.toast_queue.push_back((ToastKind::Error, error));
                }

                self.update_save = false;
            }
        }

        // Display toasts
        let window_size = frame.info().window_info.size;
        let mut toasts = Toasts::new()
            .anchor((window_size.x - 10.0, window_size.y - 10.0))
            .direction(egui::Direction::BottomUp)
            .align_to_end(true);

        while let Some((kind, text)) = self.toast_queue.pop_front() {
            toasts.add(Toast {
                kind,
                text: text.into(),
                options: ToastOptions {
                    show_icon: true,
                    expires_at: Instant::now().checked_add(Duration::from_secs(5)),
                },
            });
        }

        toasts.show(ctx);
    }
}
