# SHAR Achievement Mod

This mod ports the [RetroAchievements](https://retroachievements.org/game/19010) for The Simpsons: Hit & Run to the PC version of the game.

![Screenshot](https://gitlab.com/Omnisthetics/shar_achievements_mod/-/raw/master/screenshot.png)

## Limitations

- No RetroAchievements integration. This will not unlock RetroAchievements on your account nor will it sync up with your RetroAchievements progress.
- I Can't See Anything achievement isn't included as the PC version of SHAR doesn't have the drunk driving cheat AFAIK.
- The Scrapbook data is used for most achievements so injecting the DLL while loading/playing a partially complete save will automatically unlock some achievements.

## Usage

1. [Download](https://gitlab.com/Omnisthetics/shar_achievements_mod/-/releases) (or compile) shar_achievements.dll and shar_achievements.exe and place them in your SHAR installation directory.
2. Run SHAR.
3. Run shar_achievements.exe.
4. Once in the main menu or in a level in SHAR, click the 'Inject DLL' button in the GUI.

After injecting the DLL, achievements should unlock automatically once their conditions are fulfilled. Achievements can also be manually toggled by double-clicking them in the GUI. 

## Compilation

    cargo build --target i686-pc-windows-gnu --release

Or if you have [just](https://github.com/casey/just) installed:

    just release

## TODOs (maybe)

- Add achievement icons to GUI
- Make DLL self injecting through DirectInput 8 hooking
- Replace GUI exe with ingame overlay that is displayed via DX9 hooking

## Credits

- [zxmega](https://retroachievements.org/user/zxmega) for making the RetroAchievements.
- [seal9055](https://seal9055.com/) for their article [Hacking Dark Souls 3](https://seal9055.com/blog/game/dark_souls_3) which served as my primary learning resource and inspiration for this project.
- [Lucas Cardellini](https://forum.donutteam.com/@lucasc190) for their [SHAR Map Data Viewer](https://modbakery.donutteam.com/releases/view/lucas-map-data-viewer).
- [Cheat Engine](https://cheatengine.org/) for making pointer mapping and memory viewing a breeze.

## License

Licensed under [GNU GPLv3](https://gitlab.com/Omnisthetics/shar_achievements_mod/-/raw/master/LICENSE).
