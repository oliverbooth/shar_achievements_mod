use crate::memory::{
    get_dma_address, read_dm, read_dm_or_default, read_mem, read_mem_or_default,
    CHAR_SHEET_ADDRESS, PROGRAM_BASE_ADDRESS,
};
use serde::{Deserialize, Serialize};
use std::ffi::CStr;

const MAX_LEVELS: usize = 7;
const MAX_CARDS: usize = 7;
const MAX_MISSIONS: usize = 8;
const MAX_STREET_RACES: usize = 3;

const LEVEL_RECORD_SIZE: usize = GAGS_FOUND_OFFSET + 52;
const LEVEL_LIST_OFFSET: usize = 16;
const LEVEL_LIST_SIZE: usize = LEVEL_RECORD_SIZE * MAX_LEVELS;

const CARD_RECORD_SIZE: usize = 17;
const CARD_LIST_OFFSET: usize = 0;
const CARD_LIST_SIZE: usize = MAX_CARDS * CARD_RECORD_SIZE + 1;

const MISSION_RECORD_SIZE: usize = 32;
const MISSION_LIST_OFFSET: usize = CARD_LIST_OFFSET + CARD_LIST_SIZE;
const MISSION_LIST_SIZE: usize = MAX_MISSIONS * MISSION_RECORD_SIZE;

const STREET_RACE_RECORD_SIZE: usize = MISSION_RECORD_SIZE;
const STREET_RACE_LIST_OFFSET: usize = MISSION_LIST_OFFSET + MISSION_LIST_SIZE;
const STREET_RACE_LIST_SIZE: usize = MAX_STREET_RACES * STREET_RACE_RECORD_SIZE;

const BONUS_MISSION_RECORD_SIZE: usize = MISSION_RECORD_SIZE;
const BONUS_MISSION_RECORD_OFFSET: usize = STREET_RACE_LIST_OFFSET + STREET_RACE_LIST_SIZE;

const WAGER_RACE_RECORD_SIZE: usize = MISSION_RECORD_SIZE;
const WAGER_RACE_RECORD_OFFSET: usize = BONUS_MISSION_RECORD_OFFSET + BONUS_MISSION_RECORD_SIZE;

const FMV_UNLOCKED_OFFSET: usize = WAGER_RACE_RECORD_OFFSET + WAGER_RACE_RECORD_SIZE;
const CARS_PURCHASED_OFFSET: usize = FMV_UNLOCKED_OFFSET + 4;
const SKINS_PURCHASED_OFFSET: usize = CARS_PURCHASED_OFFSET + 4;
const WASPS_DESTROYED_OFFSET: usize = SKINS_PURCHASED_OFFSET + 4;
const GAGS_FOUND_OFFSET: usize = WASPS_DESTROYED_OFFSET + 20;

const CURRENT_LEVEL_OFFSET: usize = LEVEL_LIST_SIZE + 16;
const CURRENT_MISSION_OFFSET: usize = CURRENT_LEVEL_OFFSET + 4;
const _HIGHEST_LEVEL_OFFSET: usize = CURRENT_MISSION_OFFSET + 4;
const _HIGHEST_MISSION_OFFSET: usize = _HIGHEST_LEVEL_OFFSET + 4;

const _COINS_OFFSET: usize = _HIGHEST_MISSION_OFFSET + 8;

static mut NEW_BONUS_GAME: bool = false;

static mut SURF_VEHICLE_POS: Option<(f32, f32, f32)> = None;

static mut BLUE_FLAME_FAILED: bool = false;
static mut MONORAIL_FAILED: bool = false;
static mut KNIGHTBOAT_FAILED: bool = false;
static mut QUAD_BIKE_FAILED: bool = false;
static mut MONSTER_TRUCK_FAILED: bool = false;
static mut PINK_CAR_FAILED: bool = false;
static mut RC_CAR_FAILED: bool = false;

static mut FIRE_TRUCK_FAILED: bool = false;
static mut DONUT_TRUCK_FAILED: bool = false;
static mut DUFF_TRUCK_FAILED: bool = false;
static mut COLA_TRUCK_FAILED: bool = false;
static mut PLOW_KING_FAILED: bool = false;
static mut ARMOURED_TRUCK_FAILED: bool = false;

#[derive(Serialize, Deserialize)]
pub struct Achievement {
    pub name: String,
    pub unlocked: bool,
    #[serde(skip)]
    conditions: Vec<Condition>,
}

impl Achievement {
    /// Checks/updates the achievement's conditions and unlocks achievement if all
    /// conditions are fulfilled.
    pub fn update(&mut self) {
        if self.unlocked {
            return;
        }

        self.conditions.iter_mut().for_each(|c| c.update());

        // Unlock achievement and reset all condition hit counts if all conditions are fulfilled
        if self.conditions.iter().all(|c| c.fulfilled()) {
            self.unlocked = true;
            self.conditions.iter_mut().for_each(|c| c.hits = 0);
        }
    }

    /// Creates a new achievement from a given name and set of conditions.
    fn new(name: &str, conditions: Vec<Condition>) -> Self {
        Achievement {
            name: name.into(),
            unlocked: false,
            conditions,
        }
    }

    /// Creates a new achievement that requires the player to beat a specific
    /// mission.
    pub unsafe fn mission(name: &str, level: usize, mission: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let mission = mission.min(MAX_MISSIONS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + MISSION_LIST_OFFSET
            + (MISSION_RECORD_SIZE * (mission - 1))
            + 16;
        let closure = move || read_mem_or_default(address);
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to beat the bonus
    /// mission in a given level.
    pub unsafe fn bonus_mission(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + BONUS_MISSION_RECORD_OFFSET
            + 16;
        let closure = move || read_mem_or_default(address);
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to beat all street
    /// races in a given level.
    pub unsafe fn street_races(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let closure = move || {
            (0..MAX_STREET_RACES)
                .map(|i| {
                    CHAR_SHEET_ADDRESS
                        + LEVEL_LIST_OFFSET
                        + (LEVEL_RECORD_SIZE * (level - 1))
                        + STREET_RACE_LIST_OFFSET
                        + (STREET_RACE_RECORD_SIZE * i)
                        + 16
                })
                .map(|a| read_mem_or_default(a))
                .all(|b| b)
        };
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to beat the wager
    /// race in a given level.
    pub unsafe fn wager_race(name: &str, level: usize, seconds: u32) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + WAGER_RACE_RECORD_OFFSET
            + 28;
        let closure = move || read_mem(address).unwrap_or(u32::MAX) <= seconds;
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to collect all the
    /// cards in a given level.
    pub unsafe fn cards(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let closure = move || {
            (0..MAX_CARDS)
                .map(|i| {
                    CHAR_SHEET_ADDRESS
                        + LEVEL_LIST_OFFSET
                        + (LEVEL_RECORD_SIZE * (level - 1))
                        + (CARD_RECORD_SIZE * i)
                })
                .map(|a| CStr::from_ptr(a as *const i8).to_str() == Ok("Cardx"))
                .all(|b| b)
        };
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to purchase all the
    /// vehicles available in a given level.
    pub unsafe fn vehicles(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + CARS_PURCHASED_OFFSET;
        let closure = move || read_mem_or_default::<u32>(address) == 3;
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to purchase all the
    /// outfits/skins available in a given level.
    pub unsafe fn skins(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + SKINS_PURCHASED_OFFSET;
        let closure = move || read_mem_or_default::<u32>(address) == 3;
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to destroy all the
    /// wasps in a given level.
    pub unsafe fn wasps(name: &str, level: usize) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + WASPS_DESTROYED_OFFSET;
        let closure = move || read_mem_or_default::<u32>(address) == 20;
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to find all the gags
    /// in a given level.
    pub unsafe fn gags(name: &str, level: usize, amount: u32) -> Self {
        let level = level.min(MAX_LEVELS);
        let address = CHAR_SHEET_ADDRESS
            + LEVEL_LIST_OFFSET
            + (LEVEL_RECORD_SIZE * (level - 1))
            + GAGS_FOUND_OFFSET;
        let closure = move || read_mem_or_default::<u32>(address) == amount;
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player to beat a given
    /// bonus game track when set to 3 laps.
    pub unsafe fn bonus_game(name: &str, level: u32) -> Self {
        let level = level.min(MAX_LEVELS as u32);
        let closure = move || {
            // Get selected bonus game level
            // Bonus levels have their own entries in the LevelEnum starting at 8
            let bonus_level: u32 = read_dm_or_default(PROGRAM_BASE_ADDRESS + 0x002C87B4, &[0x38]);

            if bonus_level != level + (MAX_LEVELS as u32) {
                return false;
            }

            let player_lap_address = get_dma_address(
                PROGRAM_BASE_ADDRESS + 0x002C9018,
                &[0x414, 0x0, 0x1C, 0x28, 0x0, 0x14, 0xC],
            );

            // Get number of complete laps driven by player
            let laps: u8 = player_lap_address
                .and_then(|a| read_mem(a))
                .unwrap_or_default();

            // Get final position of player
            let position: u8 = player_lap_address
                .and_then(|a| read_mem(a + 1))
                .unwrap_or_default();

            // Bonus game doesn't clear its player stats until a new race starts so we
            // keep track of a 'dirty flag' to prevent multiple achievements potentially
            // being unlocked from one race
            if laps == 3 && position == 1 && NEW_BONUS_GAME {
                NEW_BONUS_GAME = false;
                true
            } else if laps == 0 && position == 0 {
                NEW_BONUS_GAME = true;
                false
            } else {
                false
            }
        };
        let condition = Condition {
            closure: Box::new(closure),
            hits_required: 1,
            hits: 0,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }

    /// Creates a new achievement that requires the player beat a mission with
    /// only a specific vehicle.
    pub unsafe fn vehicle_restricted_mission(
        name: &str,
        level: u32,
        mission: u32,
        vehicle_id: u32,
        fail_flag: &'static mut bool,
    ) -> Self {
        let level = level.clamp(1, MAX_LEVELS as u32).saturating_sub(1);
        let mission = mission.clamp(1, MAX_MISSIONS as u32).saturating_sub(1);

        let closure = move || {
            // Check if in mission
            let level_address = CHAR_SHEET_ADDRESS + CURRENT_LEVEL_OFFSET;
            let current_level: u32 = read_mem_or_default(level_address);

            let mission_address = CHAR_SHEET_ADDRESS + CURRENT_MISSION_OFFSET;
            let current_mission: u32 = read_mem_or_default(mission_address);

            if current_level != level || current_mission != mission {
                *fail_flag = false;

                return false;
            }

            // 4 bytes are added to the offset to skip over the setup mission for the first
            // mission of each level.
            // Level bytes are multiplied by 2 to skip over setup/intermission missions.
            let mission_offset: usize = 4 + (mission as usize * 4 * 2);

            // "Simpsons.exe"+0x002C8994 = GameplayManager singleton address
            // 0x424 = Mission array offset
            // 0x8C = Mission over flag offset
            let over_address = get_dma_address(
                PROGRAM_BASE_ADDRESS + 0x002C8994,
                &[0x424 + mission_offset, 0x8C],
            );
            let state_address = over_address.map(|a| a + 0xC);

            // Check if mission is in progress and hasn't been failed
            let mission_state: u32 = state_address.and_then(|a| read_mem(a)).unwrap_or_default();

            if mission_state != 1 {
                *fail_flag = false;

                return false;
            }

            // Check if player is driving the right vehicle
            let current_vehicle_id: u32 =
                read_dm(PROGRAM_BASE_ADDRESS + 0x002C84E0, &[0x5C]).unwrap_or(96);

            if current_vehicle_id != vehicle_id {
                *fail_flag = true;

                return false;
            }

            // Check if mission is over (i.e. "Mission Complete" is visible)
            let mission_over: bool = over_address.and_then(|a| read_mem(a)).unwrap_or_default();

            mission_over && !(*fail_flag)
        };

        let condition = Condition {
            closure: Box::new(closure),
            hits: 0,
            hits_required: 1,
            reset_hits_on_fail: false,
        };

        Achievement::new(name, vec![condition])
    }
}

struct Condition {
    closure: Box<dyn FnMut() -> bool>,
    hits_required: u32,
    hits: u32,
    reset_hits_on_fail: bool,
}

impl Condition {
    fn update(&mut self) {
        if (self.closure)() {
            self.hits += 1;
        } else if self.reset_hits_on_fail {
            self.hits = 0;
        }
    }

    fn fulfilled(&self) -> bool {
        self.hits >= self.hits_required
    }
}

/// Creates a vector of achievements based on the RetroAchievements list
/// (sans I Can't See Anything)
pub unsafe fn init_achievements() -> Vec<Achievement> {
    vec![
        Achievement::mission("The Cola Caper", 1, 1),
        Achievement::mission("S-M-R-T", 1, 2),
        Achievement::mission("Petty Theft Homer", 1, 3),
        Achievement::mission("Office Spaced", 1, 4),
        Achievement::mission("Blind Big Brother", 1, 5),
        Achievement::mission("Flowers by Irene", 1, 6),
        Achievement::mission("Bonestorm Storm", 1, 7),
        Achievement::mission("The Fat and Furious", 1, 8),
        Achievement::mission("Detention Deficit Disorder", 2, 1),
        Achievement::mission("Weapons of Mass Deliquency", 2, 2),
        Achievement::mission("Vox Nerduli", 2, 3),
        Achievement::mission("Bart 'n' Frink", 2, 4),
        Achievement::mission("Better Than Beef", 2, 5),
        Achievement::mission("Monkey See Monkey D'oh", 2, 6),
        Achievement::mission("Cell-Outs", 2, 7),
        Achievement::mission("Nerd Race Queen", 3, 1),
        Achievement::mission("Clueless", 3, 2),
        Achievement::mission("Bonfire of the Manatees", 3, 3),
        Achievement::mission("Operation Hellfish", 3, 4),
        Achievement::mission("Slithery Sleuthing", 3, 5),
        Achievement::mission("Fishy Deals", 3, 6),
        Achievement::mission("The Old Pirate and the Sea", 3, 7),
        Achievement::mission("For a Few Donuts More", 4, 1),
        Achievement::mission("Redneck Roundup", 4, 2),
        Achievement::mission("Ketchup Logic", 4, 3),
        Achievement::mission("Return of the Nearly Dead", 4, 4),
        Achievement::mission("Wolves Stole My Pills", 4, 5),
        Achievement::mission("The Cola Wars", 4, 6),
        Achievement::mission("From Outer Space", 4, 7),
        Achievement::mission("Incriminating Caffeine", 5, 1),
        Achievement::mission("...And Baby Makes 8", 5, 2),
        Achievement::mission("Eight is Too Much", 5, 3),
        Achievement::mission("This Little Piggy", 5, 4),
        Achievement::mission("Never Trust a Snake", 5, 5),
        Achievement::mission("Kwik Cash", 5, 6),
        Achievement::mission("Curious Curator", 5, 7),
        Achievement::mission("Going to the Lu'", 6, 1),
        Achievement::mission("Getting Down with the Clown", 6, 2),
        Achievement::mission("Lab Coat Caper", 6, 3),
        Achievement::mission("Duff for Me, Duff for You", 6, 4),
        Achievement::mission("Full Metal Jackass", 6, 5),
        Achievement::mission("Set to Kill", 6, 6),
        Achievement::mission("Kang and Kodos Strike Back", 6, 7),
        Achievement::mission("Rigor Motors", 7, 1),
        Achievement::mission("Long Black Probes", 7, 2),
        Achievement::mission("Pocket Protector", 7, 3),
        Achievement::mission("There's Something About Monty", 7, 4),
        Achievement::mission("Alien \"Auto\"topsy Part I", 7, 5),
        Achievement::mission("Alien \"Auto\"topsy Part II", 7, 6),
        Achievement::mission("Alien \"Auto\"topsy Part III", 7, 7),
        Achievement::bonus_mission("This Old Shanty", 1),
        Achievement::bonus_mission("Dial B for Blood", 2),
        Achievement::bonus_mission("Princi-Pal", 3),
        Achievement::bonus_mission("Beached Love", 4),
        Achievement::bonus_mission("Kinky Frinky", 5),
        Achievement::bonus_mission("Milking the Pigs", 6),
        Achievement::bonus_mission("Flaming Tires", 7),
        Achievement::street_races("Daytime Suburb Racing", 1),
        Achievement::street_races("Daytime Downtown Racing", 2),
        Achievement::street_races("Daytime Squidport Racing", 3),
        Achievement::street_races("Night Suburb Racing", 4),
        Achievement::street_races("Night Downtown Racing", 5),
        Achievement::street_races("Night Squidport Racing", 6),
        Achievement::street_races("Halloween Racing", 7),
        Achievement::vehicles("Surveillance Van", 1),
        Achievement::vehicles("WWII Vehicle", 2),
        Achievement::vehicles("Book Burning Van", 3),
        Achievement::vehicles("Krusty's Limo", 4),
        Achievement::vehicles("El Carro Loco", 5),
        Achievement::vehicles("36 Stutz Bearcat", 6),
        Achievement::vehicles("Hearse", 7),
        Achievement::skins("King Size Homer", 1),
        Achievement::skins("Bart Star", 2),
        Achievement::skins("Summer of 4 ft.2", 3),
        Achievement::skins("Scenes from the Class Struggle in Springfield", 4),
        Achievement::skins("Much Apu About Nothing", 5),
        Achievement::skins("Bartman #1", 6),
        Achievement::skins("Tree House of Horror - 4", 7),
        Achievement::wasps("Wasp Killer I", 1),
        Achievement::wasps("Wasp Killer II", 1),
        Achievement::wasps("Wasp Killer III", 1),
        Achievement::wasps("Wasp Killer IV", 1),
        Achievement::wasps("Wasp Killer V", 1),
        Achievement::wasps("Wasp Killer VI", 1),
        Achievement::wasps("Wasp Killer VII", 1),
        Achievement::gags("Homer Loves Flanders", 1, 15),
        Achievement::gags("Bart on the Road", 2, 11),
        Achievement::gags("Three Men and a Comic Book", 3, 11),
        Achievement::gags("The Old Man and the Key", 4, 15),
        Achievement::gags("Flaming Moe's", 5, 6),
        Achievement::gags("Krusty Gets Busted", 6, 11),
        Achievement::gags("Treehouse of Horror", 7, 15),
        Achievement::cards("Mr. Sparkle Box", 1),
        Achievement::cards("Head of Jebediah", 2),
        Achievement::cards("Soy Pop", 3),
        Achievement::cards("Boudoir Album", 4),
        Achievement::cards("Ganesh Costume", 5),
        Achievement::cards("Radioactive Man #1", 6),
        Achievement::cards("Soul Donut", 7),
        Achievement::wager_race("Wager Racing I", 1, 195),
        Achievement::wager_race("Wager Racing II", 2, 130),
        Achievement::wager_race("Wager Racing III", 3, 140),
        Achievement::wager_race("Wager Racing IV", 4, 150),
        Achievement::wager_race("Wager Racing V", 5, 125),
        Achievement::wager_race("Wager Racing VI", 6, 115),
        Achievement::wager_race("Wager Racing VII", 7, 90),
        Achievement::bonus_game("Daytime Suburb Super Sprint", 1),
        Achievement::bonus_game("Daytime Downtown Super Sprint", 2),
        Achievement::bonus_game("Daytime Squidport Super Sprint", 3),
        Achievement::bonus_game("Night Suburb Super Sprint", 4),
        Achievement::bonus_game("Night Downtown Super Sprint", 5),
        Achievement::bonus_game("Night Squidport Super Sprint", 6),
        Achievement::bonus_game("Halloween Super Sprint", 7),
        Achievement::new(
            "Car Surfing",
            vec![Condition {
                closure: Box::new(|| car_surfing()),
                hits_required: 6,
                hits: 0,
                reset_hits_on_fail: true,
            }],
        ),
        Achievement::new(
            "Matt Groening Gaming",
            vec![Condition {
                closure: Box::new(|| matt_groening_gaming()),
                hits_required: 1,
                hits: 0,
                reset_hits_on_fail: false,
            }],
        ),
        Achievement::new(
            "The Simp",
            vec![Condition {
                closure: Box::new(|| the_simp()),
                hits_required: 1,
                hits: 0,
                reset_hits_on_fail: false,
            }],
        ),
        Achievement::new(
            "500-Yard Gash",
            vec![Condition {
                closure: Box::new(|| {
                    let address = CHAR_SHEET_ADDRESS
                        + LEVEL_LIST_OFFSET
                        + (LEVEL_RECORD_SIZE * 2)
                        + FMV_UNLOCKED_OFFSET;

                    read_mem_or_default(address)
                }),
                hits_required: 1,
                hits: 0,
                reset_hits_on_fail: false,
            }],
        ),
        Achievement::vehicle_restricted_mission(
            "1970 Blue Flame",
            1,
            8,
            80,
            &mut BLUE_FLAME_FAILED,
        ),
        Achievement::vehicle_restricted_mission(
            "Marge vs. The Monorail",
            2,
            7,
            78,
            &mut MONORAIL_FAILED,
        ),
        Achievement::vehicle_restricted_mission(
            "Knightboat: The Crime Solving Boat",
            3,
            7,
            77,
            &mut KNIGHTBOAT_FAILED,
        ),
        Achievement::vehicle_restricted_mission("Quad Bike", 4, 4, 74, &mut QUAD_BIKE_FAILED),
        Achievement::vehicle_restricted_mission(
            "Monster Truck",
            5,
            2,
            79,
            &mut MONSTER_TRUCK_FAILED,
        ),
        Achievement::vehicle_restricted_mission("Unfunny Pink Car", 6, 2, 76, &mut PINK_CAR_FAILED),
        Achievement::vehicle_restricted_mission("Radio-Controlled", 7, 4, 75, &mut RC_CAR_FAILED),
        Achievement::vehicle_restricted_mission(
            "Fire Truck Challenge",
            3,
            6,
            92,
            &mut FIRE_TRUCK_FAILED,
        ),
        Achievement::vehicle_restricted_mission(
            "Donut Challenge",
            4,
            1,
            48,
            &mut DONUT_TRUCK_FAILED,
        ),
        Achievement::vehicle_restricted_mission("Duff Challenge", 4, 7, 65, &mut DUFF_TRUCK_FAILED),
        Achievement::vehicle_restricted_mission("Cola Challenge", 6, 4, 23, &mut COLA_TRUCK_FAILED),
        Achievement::vehicle_restricted_mission(
            "Plow King Challenge",
            6,
            5,
            58,
            &mut PLOW_KING_FAILED,
        ),
        Achievement::vehicle_restricted_mission(
            "Armored Challenge",
            7,
            1,
            40,
            &mut ARMOURED_TRUCK_FAILED,
        ),
    ]
}

/// Logic for Matt Groening Gaming achievement
unsafe fn matt_groening_gaming() -> bool {
    //Check if in Homer 1
    let level_addr = CHAR_SHEET_ADDRESS + CURRENT_LEVEL_OFFSET;
    let level: u32 = read_mem_or_default(level_addr);

    if level != 0 {
        return false;
    }

    //Check if in tutorial, 1-1 or 1-6
    let mission_addr = CHAR_SHEET_ADDRESS + CURRENT_MISSION_OFFSET;
    let mission: u32 = read_mem_or_default(mission_addr);

    if ![0, 1, 6].contains(&mission) {
        return false;
    }

    //Check if Marge is in front of the Kwik-E-Mart
    let pos_addr = get_dma_address(
        PROGRAM_BASE_ADDRESS + 0x002C8468,
        &[0x18, 0x108, 0x10, 0x10, 0x48],
    );
    let x: f32 = pos_addr.and_then(|a| read_mem(a)).unwrap_or_default();
    let z: f32 = pos_addr.and_then(|a| read_mem(a + 8)).unwrap_or_default();

    (190.0..=225.0).contains(&x) && (-310.0..=-285.0).contains(&z)
}

/// Logic for The Simp achievement
unsafe fn the_simp() -> bool {
    // Check if in Lisa's level
    let level_addr = CHAR_SHEET_ADDRESS + CURRENT_LEVEL_OFFSET;
    let level: u32 = read_mem_or_default(level_addr);

    if level != 2 {
        return false;
    }

    // Check if in 2-2
    let mission_addr = CHAR_SHEET_ADDRESS + CURRENT_MISSION_OFFSET;
    let mission: u32 = read_mem_or_default(mission_addr);

    if mission != 1 {
        return false;
    }

    // Check if Milhouse is down in the cliff by the Springfield sign
    let pos_addr = get_dma_address(
        PROGRAM_BASE_ADDRESS + 0x002C8468,
        &[0x18, 0x108, 0x10, 0x10, 0x48],
    );
    let x: f32 = pos_addr.and_then(|a| read_mem(a)).unwrap_or_default();
    let y: f32 = pos_addr.and_then(|a| read_mem(a + 4)).unwrap_or_default();
    let z: f32 = pos_addr.and_then(|a| read_mem(a + 8)).unwrap_or_default();

    (-100.0..=-75.0).contains(&x) && y <= -75.0 && (270.0..=294.0).contains(&z)
}

/// Logic for Car Surfing achievement
unsafe fn car_surfing() -> bool {
    // Check if player is standing on a vehicle
    let standing_on_vehicle: bool = read_dm_or_default(
        PROGRAM_BASE_ADDRESS + 0x002C922C,
        &[0x70, 0x8, 0x100, 0x8, 0x9A],
    );

    if !standing_on_vehicle {
        SURF_VEHICLE_POS = None;

        return false;
    }

    // Get current position of vehicle
    let vehicle_pos = {
        let address = get_dma_address(PROGRAM_BASE_ADDRESS + 0x002C922C, &[0x2E4]);
        let x: f32 = address.and_then(|a| read_mem(a)).unwrap_or_default();
        let y: f32 = address.and_then(|a| read_mem(a + 4)).unwrap_or_default();
        let z: f32 = address.and_then(|a| read_mem(a + 8)).unwrap_or_default();

        (x, y, z)
    };

    if vehicle_pos == (0.0, 0.0, 0.0) {
        SURF_VEHICLE_POS = None;

        return false;
    }

    // Check if vehicle has moved since last check
    let uncached = SURF_VEHICLE_POS.is_none();
    let stationary = SURF_VEHICLE_POS == Some(vehicle_pos);

    SURF_VEHICLE_POS = Some(vehicle_pos);

    uncached || !stationary
}
