use std::{mem::size_of, ptr::null_mut};

use winapi::{
    ctypes::c_void,
    um::{memoryapi::ReadProcessMemory, processthreadsapi::GetCurrentProcess},
};

pub const CHAR_SHEET_PTR_ADDR: usize = 0x002C8984;
pub const CHAR_SHEET_PTR_OFFSETS: [usize; 1] = [0x4];

pub static mut PROGRAM_BASE_ADDRESS: usize = 0;
pub static mut CHAR_SHEET_ADDRESS: usize = 0;

pub unsafe fn get_dma_address(pointer: usize, offsets: &[usize]) -> Option<usize> {
    let mut address = pointer;

    if address == 0 {
        return None;
    }

    for offset in offsets {
        let result = ReadProcessMemory(
            GetCurrentProcess(),
            address as *const c_void,
            (&address as *const usize) as *mut c_void,
            size_of::<usize>(),
            null_mut(),
        );

        if result == 0 || address == 0 {
            return None;
        }

        address += offset;
    }

    Some(address)
}

pub unsafe fn read_mem<T: Copy + Default>(address: usize) -> Option<T> {
    let buffer = T::default();
    let pointer: *const T = &buffer;

    if ReadProcessMemory(
        GetCurrentProcess(),
        address as *const c_void,
        pointer as *mut c_void,
        size_of::<T>(),
        null_mut(),
    ) == 0
    {
        return None;
    };

    Some(buffer)
}

pub unsafe fn read_mem_or_default<T: Copy + Default>(address: usize) -> T {
    read_mem::<T>(address).unwrap_or_default()
}

pub unsafe fn read_dm<T: Copy + Default>(pointer: usize, offsets: &[usize]) -> Option<T> {
    get_dma_address(pointer, offsets).and_then(|addr| read_mem::<T>(addr))
}

pub unsafe fn read_dm_or_default<T: Copy + Default>(pointer: usize, offsets: &[usize]) -> T {
    read_dm::<T>(pointer, offsets).unwrap_or_default()
}
