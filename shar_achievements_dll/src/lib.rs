use achievements::Achievement;
use flexi_logger::{FileSpec, Logger, WriteMode};
use memory::*;
use std::{
    ffi::CString,
    fs::{read_to_string, File},
    io::Write,
    mem,
    ptr::null_mut,
    thread,
    time::Duration,
};
use winapi::um::{
    libloaderapi::GetModuleHandleA,
    processthreadsapi::GetCurrentProcess,
    psapi::{GetModuleInformation, MODULEINFO},
};

mod achievements;
mod memory;

#[ctor::ctor]
unsafe fn ctor() {
    thread::spawn(|| initialise());
}

unsafe fn initialise() {
    let _logger = Logger::try_with_str("info, my::critical::module=trace")
        .unwrap()
        .log_to_file(
            FileSpec::default()
                .basename("shar_achievements")
                .suppress_timestamp(),
        )
        .write_mode(WriteMode::BufferAndFlush)
        .start();

    // Get base address of 'Simpsons.exe'
    PROGRAM_BASE_ADDRESS = {
        let module_handle = {
            let program_name = CString::new("Simpsons.exe").unwrap();

            GetModuleHandleA(program_name.as_c_str().as_ptr())
        };
        let mut module_info = MODULEINFO {
            EntryPoint: null_mut(),
            SizeOfImage: 0,
            lpBaseOfDll: null_mut(),
        };
        let size = mem::size_of::<MODULEINFO>() as u32;

        GetModuleInformation(GetCurrentProcess(), module_handle, &mut module_info, size);

        module_info.lpBaseOfDll as usize
    };

    // Traverse pointer chain to get the character sheet address
    CHAR_SHEET_ADDRESS = get_dma_address(
        PROGRAM_BASE_ADDRESS + CHAR_SHEET_PTR_ADDR,
        &CHAR_SHEET_PTR_OFFSETS,
    )
    .expect("Failed to get character sheet address.");

    let mut achievements = achievements::init_achievements();

    loop {
        // Sync achievement state with save file
        let saved_achievements: Vec<Achievement> = read_to_string("achievements.ron")
            .ok()
            .and_then(|s| ron::from_str(&s).ok())
            .unwrap_or_default();

        achievements.iter_mut().for_each(|a| {
            a.unlocked = saved_achievements
                .iter()
                .find(|sa| a.name == sa.name)
                .map(|sa| sa.unlocked)
                .unwrap_or_else(|| a.unlocked);
        });

        let mut update_save = false;

        // Update currently locked achievements
        for achievement in achievements.iter_mut().filter(|a| !a.unlocked) {
            achievement.update();

            if achievement.unlocked {
                update_save = true;
            }
        }

        // Update achievement save file if an achievement was unlocked this cycle
        if update_save {
            use ron::ser as ron;

            if let Ok(serialised_achievements) =
                ron::to_string_pretty(&achievements, ron::PrettyConfig::default())
            {
                let _ = File::create("achievements.ron")
                    .and_then(|mut f| f.write_all(serialised_achievements.as_bytes()));
            }
        }

        //WARNING: Some achievement conditions may depend on more frequent updates.
        //         Lower duration when necessary and adjust achievement condition hits accordingly.
        thread::sleep(Duration::from_secs(1));
    }
}
